# Django CRM Project - Contact Management Project 

## Live Demo - https://eterie-app.herokuapp.com/


![Image of CRM](https://gitlab.com/danthuyvo.dev/crm/-/raw/main/views/dashboard.png)


### Demo Login

- Username : live_demo
- Password  : Password1234__  

### Build with

- Python 3.8.5
- PostgreSQL / MySQL
- Django 



### Setup
1. Create a folder and put all the files inside it.
2. Create a virtual environtment - `virtualenv env`
3. Activate VirtualENV - ubuntu : `source env/bin/activate` || windows : `. .\env\Scripts\activate`
4. Run requirements.txt - `pip install -r requirements.txt`
5. Migrate
6. Run the Application - `python manage.py runserver`


Install : 'sudo apt-get install libpq-dev'
 
Stop server : lsof -t -i tcp:8000 | xargs kill -9



This is the series of Django CRM Tutorial.

https://studygyaan.com/build-django-crm-project-from-scratch


